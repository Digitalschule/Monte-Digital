# Monte-Digital

Die Gruppe *Monte-Digital* ist ein Zusammenschluss von Menschen, die sich beruflich und/oder in ihrer Freizeit mit digitalen Themen befassen und ihre Kompetenzen gern mit dem Schulumfeld teilen möchten.  
Generell sind alle willkommen, Eltern und Erziehungsberechtigte, Lehrende und natürlich auch Schülerinnen und Schüler.

An dieser Stelle entsteht die Dokumentation unseres Tuns.

## Erreichbarkeit

Wir sind per **E-Mail** erreichbar: monte_digital@posteo.de

Wir haben eine **Mailingliste**, auf der sich Interessierte selbst eintragen können: https://lists.posteo.de/listinfo/monte_digital.

Die Mailingliste nutzen wir, um Informationen innerhalb der *Monte-Digital*-Interessierten zu verbreiten, beispielsweise über unsere regelmäßig stattfindenden **Online-Treffen**.

## Rechtlicher Hinweis
Unsere Gruppierung ist ein privater Zusammenschluss von Ehrenamtlichen.
Eine rechtliche Verbindung zur Montessori-Schule Greifswald besteht nicht.
Wir können keine Rechtsberatung anbieten, jedoch allgemeine Grundlagen z.B. im Datenschutzrecht vermitteln.

Die Mailingliste ist ein rein privates Angebot von *Monte-Digital*.
Die E-Mail-Adressen werden beim Provider Posteo.de (Posteo e.K.; Methfesselstrasse 38; 10965 Berlin) vorgehalten.
Das Nutzen der Mailingliste macht die Daten weiterhin ausschließlich den Administratoren von *Monte-Digital* zugänglich.
Sie werden natürlich nicht weitergegeben oder für andere Zwecke verwendet.  
Beim Schreiben einer Nachricht an die Mailingliste wird Ihre E-Mail-Adresse automatisiert entfernt, sodass die anderen Teilnehmenden sie nicht bekommen.

